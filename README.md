CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------

Drupolymer is a simple module that allows
you to making Polymer elements work with the Drupal Form API.

 * Current supported elements:
    Paper Radio Group (https://www.webcomponents.org/element/PolymerElements/paper-radio-group)

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/drupolymer


REQUIREMENTS
------------

This module requires the following modules:
    1. Web components (https://www.drupal.org/project/webcomponents)
    2. Drupolymer library (https://github.com/heyMP/drupolymer)


INSTALLATION
------------

    1. Install the drupolymer module.
    2. Install the drupolymer library :
        To utilize the drupolymer library you need to add drupolymer html import
        and it's dependencies, as well as the webcomponents-lite.js polyfill to
        Drupal. The easiest way to do that is with the webcomponents module.

        Installing drupolymer library with the webcomponents module :
            1. Make sure you have bower installed on your machine
            2. Install the webcomponents module and the webcomponents_polymer submodule
            3. In your active theme, create a directory called polymer
            4. If you don't already have bower.json file create one with bower init
            5. Install the drupolymer library with bower install --save drupolymer
            6. Clear cache
    3. Clear cache, the supported form elements will now be replaced by polymer elements


MAINTAINERS
-----------

Current maintainers:
 * Michael Potter (heyMP) - https://www.drupal.org/u/heymp

This project has been sponsored by:
 * Pennsylvania State University
